import falcon, datetime
class pathHandlers:
    def on_get(self, req, resp, path):
        try:
            now = datetime.datetime.now()
            if path == "ping":
                resp.status = falcon.HTTP_200
                resp.context["response"] = "pong"
            elif path == "datetime":
                resp.status = falcon.HTTP_200
                resp.context_type = "application/json"
                dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
                resp.context["response"] = {"current" : dt_string}
            elif path == "echo":
                resp.status = falcon.HTTP_200
                resp.context["response"] = req.context['request']
            else:
                resp.status = falcon.HTTP_404
                resp.context["response"] = "404"
        except Exception as e:
                raise falcon.HTTPError(falcon.HTTP_400,
                                       description=str(e))

    def on_post(self, req, resp, path):
        try:
            now = datetime.datetime.now()
            if path == "ping":
                resp.status = falcon.HTTP_200
                resp.context["response"] = "pong"
            elif path == "datetime":
                resp.status = falcon.HTTP_200
                resp.context_type = "application/json"
                dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
                resp.context["response"] = {"current" : dt_string}
            elif path == "echo":
                resp.status = falcon.HTTP_200
                resp.context["response"] = req.context['request']
            else:
                resp.status = falcon.HTTP_404
                resp.context["response"] = "404"
        except Exception as e:
            raise falcon.HTTPError(falcon.HTTP_400,
                                   description=str(e))