import falcon
from path import pathHandlers
from middleware import JSONTranslator

def handle_404(req, resp):
    resp.status = falcon.HTTP_404
    resp.context["response"] = ":p"

def route() -> falcon.App:
    app = falcon.App(middleware=[
        JSONTranslator(),
    ])
    app.add_route('/{path}', pathHandlers())
    app.add_sink(handle_404, '')
    return app